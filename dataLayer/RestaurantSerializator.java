package dataLayer;

import java.io.*;


import businessLayer.Restaurant;

public class RestaurantSerializator {

	protected static String fileName = "restaurantConte.ser";
	public static Restaurant loadRestaurant() {
		Restaurant restaurant = null;
		try {
			FileOutputStream f = new FileOutputStream(fileName);
			ObjectOutputStream os = new ObjectOutputStream(f);

			os.writeObject(restaurant);

			os.close();
			f.close();
		} catch (IOException err) {
			System.out.println("Eroare IOException(saveRestaurant)");
			err.printStackTrace();
		}
	
		
		try {
			FileInputStream f = new FileInputStream(fileName);
			//File outputFile = new File(getFilesDir(), fileName);
			ObjectInputStream os = new ObjectInputStream(f);

			restaurant = (Restaurant) os.readObject();

			os.close();
			f.close();
			
			 } catch (IOException err) {
		 System.out.println("Eroare IOException(loadRestaurant)");
		 err.printStackTrace();
		} catch (ClassNotFoundException err) {
			System.out.println("Eroare ClassNotFound(loadRestaurant)");
			err.printStackTrace();
		}
		return restaurant;
	}

	public static void saveRestaurant(Restaurant restaurant) {
		try {
			FileOutputStream f = new FileOutputStream(fileName);
			ObjectOutputStream os = new ObjectOutputStream(f);

			os.writeObject(restaurant);

			os.close();
			f.close();
		} catch (IOException err) {
			System.out.println("Eroare IOException(saveRestaurant)");
			err.printStackTrace();
		}
	}
}
