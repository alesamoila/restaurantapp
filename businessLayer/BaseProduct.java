package businessLayer;

public class BaseProduct extends MenuItem {

	private static final long serialVersionUID = 1L;

	@Override
	public int computePrice() {
		return this.getPret();
	}

	public BaseProduct(String nume, int pret) {
		super(nume, pret);
		super.setNume(nume);
		super.setPret(pret);
	}

	
}
