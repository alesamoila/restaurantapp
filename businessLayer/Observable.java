package businessLayer;

public abstract class Observable {

	public abstract void notify(String s);
}
