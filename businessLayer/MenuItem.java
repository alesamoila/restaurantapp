package businessLayer;

import java.io.Serializable;
import java.util.*;

public abstract class MenuItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// private List<Produs> listaProduse = new ArrayList<Produs>();
	private String nume;
	private int pret;

	public MenuItem(String nume, int pret) {
		super();
		this.nume = nume;
		this.pret = pret;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public boolean isWellFormed() {
		if (this.pret <= 0)
			return false;
		else if (this.nume == null)
			return false;
		else
			return true;

	}

	public abstract int computePrice();
	
	public String toString() {
		return this.getNume();
	}
}
