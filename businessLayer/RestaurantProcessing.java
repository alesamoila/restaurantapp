package businessLayer;

import java.util.List;

public interface RestaurantProcessing {

	void adaugaProdus(MenuItem produs);

	void modificaProdus(MenuItem nemodif, MenuItem modif);

	void stergeProdus(MenuItem produs);

	void genereazaFactura(Order comanda);

	void adaugaComanda(Order comanda, List<MenuItem> produse);

	int calculeazaPretulComenzii(Order comanda);

	

}
