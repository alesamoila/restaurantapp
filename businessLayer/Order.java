package businessLayer;

import java.io.Serializable;

public class Order implements Serializable {

	private int orderID;
	private String date;
	private int table;

	public int hashCode() {
		int hc = (orderID * table) % date.length();
		return hc;
	}

	public Order(int orderID, String date, int table) {
		super();
		this.orderID = orderID;
		this.date = date;
		this.table = table;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public boolean isWellFormed() {

		if (date == null)
			return false;
		if (orderID < 0)
			return false;
		if (table < 0)
			return false;

		return true;
	}

	@Override
	public boolean equals(Object c) {
		Order comanda = (Order) c;
		return this.orderID == comanda.orderID && this.date.equals(comanda.date) && this.table == comanda.table;
	}

//	public void caluleazaTVA()
}
