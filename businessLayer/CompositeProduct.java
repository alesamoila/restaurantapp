package businessLayer;


import java.util.*;

public class CompositeProduct extends MenuItem {

	
	private static final long serialVersionUID = 1L;
	private List<MenuItem> lista = new ArrayList<MenuItem>();

	@Override
	public int computePrice()
	{
		int rezultat;
		rezultat= this.getPret();
		for(MenuItem produs: lista) {
			rezultat += produs.computePrice();
		}
		return rezultat;
	}
	public CompositeProduct(String nume, int pret, List<MenuItem> lista) {
		super(nume, pret);
		super.setNume(nume);
		super.setPret(pret);
		this.lista = lista;
	}

	public List<MenuItem> getLista() {
		return lista;
	}

	public void setLista(List<MenuItem> lista) {
		this.lista = lista;
	}

}