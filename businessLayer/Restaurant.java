package businessLayer;
import presentationLayer.Observer;

import java.io.FileWriter;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import businessLayer.Order;

public class Restaurant extends Observable implements RestaurantProcessing, Serializable {

	
	private static final long serialVersionUID = 1L;
	private List<MenuItem> meniu;
	private Map<Order, List<MenuItem>> comenzi;

	private transient Observer bucatar;

	public Restaurant() {
		
	}
	
	public Restaurant(List<MenuItem> meniu, Map<Order, List<MenuItem>> comenzi, Observer bucatar) {
		super();
		this.meniu = meniu;
		this.comenzi = comenzi;
		this.bucatar = bucatar;
	}

	public List<MenuItem> getMeniu() {
		return meniu;
	}

	public void setMeniu(List<MenuItem> meniu) {
		this.meniu = meniu;
	}

	public Map<Order, List<MenuItem>> getComenzi() {
		return comenzi;
	}

	 public Set<Order> getComenzi1(){
	        return comenzi.keySet();
	    }
	 
	public void setComenzi(Map<Order, List<MenuItem>> comenzi) {
		this.comenzi = comenzi;
	}

	public Observer getBucatar() {
		return bucatar;
	}

	public void setBucatar(Observer bucatar) {
		this.bucatar = bucatar;
	}

	public Restaurant(List<MenuItem> meniu, Map<Order, List<MenuItem>> comenzi) {
		super();
		this.meniu = meniu;
		this.comenzi = comenzi;
	}


	public boolean isWellFormed() {
		if (meniu == null)
			return false;
		for (MenuItem m : meniu) {
			if (!m.isWellFormed())
				return false;

		}
		if (comenzi == null)
			return false;
		for (Order c : comenzi.keySet()) {
			if (!c.isWellFormed())
				return false;
		}

		return true;
	}

	// metodele pentru produse
	public void adaugaProdus(MenuItem produs) {
		// pre
		assert produs != null;
		meniu.add(produs);
		// post
		boolean b = this.isWellFormed();
		assert b == true;

	}

	public void modificaProdus(MenuItem nemodif, MenuItem modif) {
		// pre
		assert nemodif != null;
		assert modif != null;
		for (MenuItem produs : meniu)
			if (produs == nemodif)
				produs = modif;
		// post
		assert this.isWellFormed();

	}

	public void stergeProdus(MenuItem produs) {
		// pre
		assert produs != null;
		meniu.remove(produs);
		// post
		assert this.isWellFormed();
	}

	// metodele pentru comenzi
	public double calculeazaTVA(Order comanda) {
		return calculeazaPretulComenzii(comanda) - calculeazaPretulComenzii(comanda) / 1.24;
	}

	public void genereazaFactura(Order comanda) {
		// pre
		assert comanda != null;
		String bonFiscal = new String();
		List<MenuItem> lista = comenzi.get(comanda);

		bonFiscal = bonFiscal + "BON FISCAL" + '\n' + "S.C. Conte S.R.L." + '\n' + "Str.Mehedinti, nr 37, Cluj-Napoca"
				+ '\n';
		bonFiscal = bonFiscal + "Cod: " + comanda.getOrderID() + '\n';
		bonFiscal = bonFiscal + "Data: " + comanda.getDate() + '\n' + '\n' + '\n';

		for (MenuItem prod : lista) {
			bonFiscal = bonFiscal + "1 x           " + prod.getPret() + '\n';
			bonFiscal = bonFiscal + prod.getNume() + '\n';

		}

		bonFiscal = bonFiscal + '\n' + '\n' + '\n';
		bonFiscal = bonFiscal + "TOTAL : " + calculeazaPretulComenzii(comanda) + '\n';
		bonFiscal = bonFiscal + "TVA: " + calculeazaTVA(comanda) + '\n' + '\n';
		bonFiscal = bonFiscal + "Masa nr " + comanda.getTable() + '\n';

		try {
			FileWriter file = new FileWriter("BonFiscal.txt");

			for (int i = 0; i < bonFiscal.length(); i++) {
				if (bonFiscal.charAt(i) == '\n')
					file.write(System.getProperty("line.separator"));
			}
		} catch (IOException err) {
			err.printStackTrace();
		}

		// post

		assert this.isWellFormed();

	}

	public void adaugaComanda(Order comanda, List<MenuItem> produse) {
		// pre
		assert comanda != null;
		assert produse != null;
		comenzi.put(comanda, produse);
		notify(produse.toString());
		// post
		assert this.isWellFormed();
	}

	public int calculeazaPretulComenzii(Order comanda) {
		// pre
		assert comanda != null;
		List<MenuItem> mancare = comenzi.get(comanda);
		int rezultat = 0;
		for (MenuItem m : mancare) {
			rezultat += m.computePrice();
		}
		// post
		assert this.isWellFormed();

		return rezultat;
	}

	@Override
	public void notify(String mesaj) {
		bucatar.afisare(mesaj);
		
	}

}
