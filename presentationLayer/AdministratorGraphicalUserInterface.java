package presentationLayer;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;


public class AdministratorGraphicalUserInterface extends JFrame{

	
	private static final long serialVersionUID = 1L;
	
	protected JPanel panel;
	protected JTable table;
	protected JLabel nume;
	protected JLabel pret,descriere,meniuL;
	protected JTextField t1,t2,t3;
	protected JButton adauga, sterge, modifica,salveaza,select;
	protected JTextArea text;
	protected JComboBox meniu;
	protected List< MenuItem> produse;
	protected String op;
	
	public AdministratorGraphicalUserInterface() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Interfata administrator");
		//this.setVisible(true);
		//this.setResizable(true);
		//setSize(700,800);
		//this.getContentPane().setLayout(new BorderLayout(0,0));
		this.setBounds(100,100,900,415);
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5,5,5,5));
	    this.setContentPane(panel);
		
		//panel = new JPanel();
		panel.setLayout(null);
		//panel.setVisible(true);
		//panel.setSize(600,700);
		
		 produse = new ArrayList<MenuItem>();
		

        adauga = new JButton("Adauga");
        adauga.setBounds(42, 23, 89, 23);
       // adauga.setVisible(true);
        panel.add(adauga);

        sterge = new JButton("Sterge");
        sterge.setBounds(321, 23, 122, 23);
        //sterge.setVisible(true);
        panel.add(sterge);
        
        modifica = new JButton("Modifica");
        modifica.setBounds(163, 23, 122, 23);
        //modifica.setVisible(true);
        panel.add(modifica);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(42, 58, 401, 282);
        panel.add(scrollPane);
        

        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scrollPane.setViewportView(table);
        table.setFillsViewportHeight(true);
        

        t1 = new JTextField();
        t1.setBounds(542, 80, 146, 20);
        //t1.setVisible(true);
        panel.add(t1);
        t1.setColumns(10);
        
       nume = new JLabel("Nume");
        nume.setBounds(453, 83, 79, 14);
        //nume.setVisible(true);
        panel.add(nume);
        
        t2 = new JTextField();
        t2.setBounds(542, 116, 146, 20);
        //t2.setVisible(true);
        panel.add(t2);
        t2.setColumns(10);

        pret = new JLabel("Pret");
        pret.setBounds(453, 119, 79, 14);
        //pret.setVisible(true);
        panel.add(pret);

        salveaza = new JButton("Submit");
        salveaza.setBounds(504, 183, 89, 23);
        //salveaza.setVisible(true);
        panel.add(salveaza);

        text = new JTextArea();
        text.setBounds(698, 47, 154, 181);
        //text.setVisible(true);
        panel.add(text);

        select = new JButton(">>");
        select.setBounds(632, 46, 56, 23);
        //select.setVisible(true);
        panel.add(select);
/*
        descriere = new JLabel("Description");
        descriere.setBounds(453, 158, 79, 14);
        //descriere.setVisible(true);
        panel.add(descriere);

        t3 = new JTextField();
        t3.setBounds(542, 152, 146, 20);
        //t3.setVisible(true);
        panel.add(t3);
        t3.setColumns(10);
*/
        meniu = new JComboBox();
        meniu.setBounds(504, 47, 118, 20);
        //meniu.setVisible(true);
        panel.add(meniu);

        meniuL = new JLabel("Meniu");
        meniuL.setBounds(453, 50, 46, 14);
        //meniuL.setVisible(true);
        panel.add(meniuL);
		
	}
	
	public JTable creareTable(List<MenuItem> meniu){
        String[] numeColoane = {"Nume", "Pret"};

        List<List<Object>> inf = new ArrayList<List<Object>>();

        for (MenuItem produs : meniu) {
            List<Object> rand = new ArrayList<Object>();

            rand.add(produs.getNume());
            rand.add(produs.getPret());

            inf.add(rand);
        }

        Object[][] randuri = new Object[inf.size()][2];
        for (int i = 0; i < inf.size(); ++i) {
            List<Object> row = inf.get(i);
            randuri[i] = row.toArray();
        }

        return new JTable(randuri, numeColoane);
    }

    public void setTable(List<MenuItem> meniu){
        this.table.setModel(creareTable(meniu).getModel());
    }

    public Order ProduseSelectate(){
        int i = table.getSelectedRow();
        if(i == -1)
            return null;

        String nume = (String)table.getValueAt(i, 0);
        int pret = (Integer)table.getValueAt(i, 1);

        return null;
    }
    
    public void updateTextFields(){
        MenuItem m = (MenuItem)meniu.getSelectedItem();

        t1.setText(m.getNume());
        t2.setText(String.valueOf(m.getPret()));
    }

    public MenuItem getProduse(){
        MenuItem produs;
        String nume = t1.getText();
        int pret = Integer.parseInt(t2.getText());
        

        if(produse.size() == 0)
            produs = new BaseProduct(nume, pret);
        else
            produs = new CompositeProduct(nume, pret, produse);

        return produs;
    }

    public void stergeTextul(){
        t1.setText("");
        t2.setText("");
        t3.setText("");
        text.setText("");
    }

    public void OperatieCurenta(String s){
        op = s;
    }
    
    public void setTextVis(boolean b){
        salveaza.setVisible(b);
        nume.setVisible(b);
        pret.setVisible(b);
        t1.setVisible(b);
        t2.setVisible(b);
        t3.setVisible(b);
    }

    public void setButoaneVis(boolean b){
        meniuL.setVisible(b);
        select.setVisible(b);
        text.setVisible(b);
        meniu.setVisible(b);
    }

    public void addAdaugaListener(ActionListener a){
        adauga.addActionListener(a);
    }

    public void addSalveazaListener(ActionListener a){
        salveaza.addActionListener(a);
    }

    public void addSelectListener(ActionListener a){
        select.addActionListener(a);
    }

    public void addModificaListener(ActionListener a){
       modifica.addActionListener(a);
    }

    public void addStergeListener(ActionListener a){
        sterge.addActionListener(a);
    }

}



