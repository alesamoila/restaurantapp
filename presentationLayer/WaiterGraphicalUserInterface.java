package presentationLayer;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.*;

import businessLayer.*;

public class WaiterGraphicalUserInterface extends JFrame {

	private static final long serialVersionUID = 1L;
	protected JPanel panel;
	protected JTable table;
	protected JTextField t1;
	protected JTextField t2;
	protected JTextField t3;
	protected JLabel idL;
	protected JLabel dataL;
	protected JLabel tableL;
	protected JButton adauga;
	protected JButton pret;
	protected JButton factura;
	protected JButton salveaza;
	protected JLabel pret1;
	protected JLabel pret2;
	protected JComboBox meniu;
	protected JTextArea text;
	protected JButton select;
	protected JLabel meniuL;
	protected List<MenuItem> produse;

	public WaiterGraphicalUserInterface() {
		setTitle("Interfata Chlener");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(150, 150, 950, 400);
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel);
		panel.setLayout(null);

		adauga = new JButton("Adauga Comanda ");
		adauga.setBounds(25, 23, 140, 30);
		panel.add(adauga);

		pret = new JButton("Pret");
		pret.setBounds(180, 23, 120, 30);
		panel.add(pret);

		factura = new JButton("Bon Fiscal");
		factura.setBounds(320, 23, 120, 30);
		panel.add(factura);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(42, 58, 401, 282);
		panel.add(scrollPane);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		table.setFillsViewportHeight(true);

		t1 = new JTextField();
		t1.setBounds(542, 90, 86, 20);
		panel.add(t1);
		t1.setColumns(10);

		idL = new JLabel("Id Comanda");////
		idL.setBounds(475, 90, 60, 14);
		panel.add(idL);

		t2 = new JTextField();
		t2.setBounds(542, 135, 86, 20);
		panel.add(t2);
		t2.setColumns(10);

		dataL = new JLabel("Data");
		dataL.setBounds(475, 135, 58, 14);
		panel.add(dataL);

		t3 = new JTextField();
		t3.setBounds(542, 180, 86, 20);
		panel.add(t3);
		t3.setColumns(10);

		tableL = new JLabel("Masa");
		tableL.setBounds(475, 180, 60, 20);
		panel.add(tableL);

		salveaza = new JButton("Salveaza");
		salveaza.setBounds(515, 240, 90, 25);
		panel.add(salveaza);

		pret1 = new JLabel("Pretul pentru comanda selectata este:");
		pret1.setBounds(455, 30, 250, 20);
		panel.add(pret1);

		pret2 = new JLabel("");
		pret2.setBounds(660, 27, 46, 14);
		panel.add(pret2);

		meniu = new JComboBox();
		meniu.setBounds(517, 52, 111, 20);
		panel.add(meniu);

		text = new JTextArea();
		text.setBounds(698, 47, 154, 181);
		panel.add(text);

		select = new JButton(">>");
		select.setBounds(638, 52, 49, 23);
		panel.add(select);

		produse = new ArrayList<MenuItem>();

		meniuL = new JLabel("Meniu");
		meniuL.setBounds(474, 52, 46, 14);
		panel.add(meniuL);
	}

	public JTable createTable(Set<Order> orders) {
		String[] columnNames = { "Id Comanda", "Data", "Masa" };

		List<List<Object>> data = new ArrayList<List<Object>>();

		for (Order order : orders) {
			List<Object> row = new ArrayList<Object>();

			row.add(order.getOrderID());
			row.add(order.getDate());
			row.add(order.getTable());

			data.add(row);
		}

		Object[][] rowData = new Object[data.size()][3];
		for (int i = 0; i < data.size(); ++i) {
			List<Object> row = data.get(i);
			rowData[i] = row.toArray();
		}

		return new JTable(rowData, columnNames);
	}

	public void setTextFieldsVisible(boolean b) {
		idL.setVisible(b);
		dataL.setVisible(b);
		tableL.setVisible(b);

		t1.setVisible(b);
		t2.setVisible(b);
		t3.setVisible(b);

		meniu.setVisible(b);
		meniuL.setVisible(b);
		select.setVisible(b);
		text.setVisible(b);
		salveaza.setVisible(b);
	}

	public void setLabelsVisible(boolean b) {
		pret1.setVisible(b);
		pret2.setVisible(b);
	}

	public void setTable(Set<Order> orders) {
		this.table.setModel(createTable(orders).getModel());
	}

	public Order getSelectedOrder() {
		int i = table.getSelectedRow();
		if (i == -1)
			return null;

		int id = (Integer) table.getValueAt(i, 0);
		String date = (String) table.getValueAt(i, 1);
		int t = (Integer) table.getValueAt(i, 2);

		return new Order(id, date, t);
	}

	public Order getEnteredOrder() {
		int id = Integer.parseInt(t1.getText());
		String date = t2.getText();
		int table = Integer.parseInt(t3.getText());

		return new Order(id, date, table);
	}

	public void stergeTextul() {
		text.setText("");
		t1.setText("");
		t2.setText("");
		t3.setText("");
	}

	public void addAdaugaListener(ActionListener a) {
		adauga.addActionListener(a);
	}

	public void addPretListener(ActionListener a) {
		pret.addActionListener(a);
	}

	public void addSelecteazaListener(ActionListener a) {
		select.addActionListener(a);
	}

	public void addSalveazaListener(ActionListener a) {
		salveaza.addActionListener(a);
	}

	public void addFacturaListener(ActionListener a) {
		factura.addActionListener(a);
	}
}
