package presentationLayer;

import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class Main {

	public static void main(String[] args) throws NullPointerException {
		// Restaurant restaurant = RestaurantSerializator.loadRestaurant();
		Restaurant restaurant = new Restaurant();
		ChefGraphicalUserInterface bucatar = new ChefGraphicalUserInterface();

		bucatar.setVisible(true);

		restaurant.setBucatar(bucatar);

		WaiterGraphicalUserInterface chelner = new WaiterGraphicalUserInterface();
		chelner.setVisible(true);
		AdministratorGraphicalUserInterface administrator = new AdministratorGraphicalUserInterface();
		administrator.setVisible(true);
		
			Controller controller = new Controller(chelner, administrator, restaurant);
		

	}
}
