package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import businessLayer.*;

import dataLayer.*;

public class Controller {

	private WaiterGraphicalUserInterface chelner;
	private AdministratorGraphicalUserInterface administrator;
	private Restaurant restaurant;

	public Controller(WaiterGraphicalUserInterface chelner, AdministratorGraphicalUserInterface aministrator,
			Restaurant restaurant) {
		this.chelner = chelner;
		this.administrator = administrator;
		this.restaurant = restaurant;

		administrator.meniu.setModel(new DefaultComboBoxModel<>(restaurant.getMeniu().toArray()));

		this.chelner.setTable(restaurant.getComenzi1());
		this.administrator.setTable(restaurant.getMeniu());
		this.administrator.setVisible(false);
		// administrator.setTextFieldsVisible(false);
		this.chelner.setTextFieldsVisible(false);
		this.chelner.setLabelsVisible(false);

		this.chelner.addAdaugaListener(new AdaugaComandaListener());
		this.chelner.addPretListener(new PretListener());
		this.chelner.addSelecteazaListener(new SelectComandaListener());
		this.chelner.addSalveazaListener(new SalveazaComandaListener());
		this.chelner.addFacturaListener(new GenereazaFacturaListener());

		//de modif
		this.administrator.addAdaugaListener(new AddItemListener());
		this.administrator.addSalveazaListener(new SubmitItemListener());
		this.administrator.addSelectListener(new SelectItemListener());
		this.administrator.addModificaListener(new EditItemListener());
		this.administrator.addStergeListener(new DeleteItemListener());
	}
	
	  public class AdaugaComandaListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            chelner.meniu.setModel(new DefaultComboBoxModel<>(restaurant.getMeniu().toArray()));

	            chelner.setTextFieldsVisible(true);
	            chelner.setLabelsVisible(false);


	        }
	    }

	    public class AddItemListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            administrator.setButoaneVis(true);
	            administrator.setTextVis(true);
	            administrator.stergeTextul();
	            administrator.OperatieCurenta("Adauga");
	        }
	    }

	    public class PretListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            Order selectedOrder = chelner.getSelectedOrder();
	            chelner.setTextFieldsVisible(false);
	            if(selectedOrder != null) {
	                chelner.setLabelsVisible(true);
	                chelner.pret1.setText(String.valueOf(restaurant.calculeazaPretulComenzii(selectedOrder)));
	            }
	        }
	    }

	    public class SelectComandaListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            MenuItem item = (MenuItem)chelner.meniu.getSelectedItem();
	            chelner.produse.add(item);
	            chelner.text.append(item.getNume() + "\n");

	        }
	    }

	    public class SelectItemListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            if(administrator.op.equals("Adauga")) {
	                MenuItem item = (MenuItem) administrator.meniu.getSelectedItem();
	                administrator.produse.add(item);
	                administrator.text.append(item.getNume() + "\n");
	            }
	        }
	    }

	    public class SalveazaComandaListener implements ActionListener{
	        public void actionPerformed(ActionEvent e) {
	            try {
	                Order enteredOrder = chelner.getEnteredOrder();

	                restaurant.adaugaComanda(enteredOrder, chelner.produse);
	                chelner.setTable((Set<Order>) restaurant.getComenzi());
	                chelner.stergeTextul();
	                RestaurantSerializator.saveRestaurant(restaurant);
	            } catch(NumberFormatException | AssertionError ex){
	                JOptionPane.showMessageDialog(chelner, "The data entered is invalid");
	            }
	        }
	    }

	    public class EditItemListener implements  ActionListener{
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            administrator.OperatieCurenta("Modifica");

	            administrator.setButoaneVis(true);
	            administrator.setTextVis(true);

	            try {
	                administrator.updateTextFields();
	            } catch (NullPointerException ex){
	                JOptionPane.showMessageDialog(administrator, "No items in the menu!");
	            }
	        }
	    }

	    public class SubmitItemListener implements ActionListener{
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            try {
	                if(administrator.op.equals("Adauga")){

	                        MenuItem item = administrator.getProduse();
	                        restaurant.adaugaProdus(item);
	                        administrator.setTable(restaurant.getMeniu());
	                        administrator.stergeTextul();
	                        administrator.meniu.setModel(new DefaultComboBoxModel<Object>(restaurant.getMeniu().toArray()));
	                        RestaurantSerializator.saveRestaurant(restaurant);

	                }
	                else if(administrator.op.equals("Modifica")){
	                    MenuItem item = (MenuItem)administrator.meniu.getSelectedItem();
	                    item.setNume(administrator.t1.getText());
	                    item.setPret(Integer.parseInt(administrator.t2.getText()));

	                    administrator.meniu.setModel(new DefaultComboBoxModel<Object>(restaurant.getMeniu().toArray()));
	                    administrator.setTable(restaurant.getMeniu());
	                    RestaurantSerializator.saveRestaurant(restaurant);
	                }
	            } catch (AssertionError error){
	                JOptionPane.showMessageDialog(administrator, "The data entered is invalid");
	            } catch (NumberFormatException ex){
	                JOptionPane.showMessageDialog(administrator, "The data entered is invalid");
	            }
	        }
	    }

	    public class DeleteItemListener implements ActionListener {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            try {
	                restaurant.stergeProdus((MenuItem) administrator.meniu.getSelectedItem());
	                administrator.setTable(restaurant.getMeniu());
	                administrator.meniu.setModel(new DefaultComboBoxModel<Object>(restaurant.getMeniu().toArray()));
	                RestaurantSerializator.saveRestaurant(restaurant);
	            } catch (AssertionError error){
	                JOptionPane.showMessageDialog(administrator, "No item in the menu !");
	            }
	        }
	    }

	    public class GenereazaFacturaListener implements ActionListener {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            try {
	                Order o = chelner.getSelectedOrder();
	                restaurant.genereazaFactura(o);
	            } catch (AssertionError error){
	                JOptionPane.showMessageDialog(chelner, "No orders in the restaurant");
	            }
	        }
	    }
	}



